# fluro angular boilerplate
Starter kit for building a fluro angular app

# Getting Started
Clone boilerplate repository
~~~~
git clone git@bitbucket.org:CadeEmbery/fluro-angular-boilerplate.git MYAPPLICATIONNAME
~~~~

Create a new repository for your project, then set remote url to new repository
~~~~
git remote set-url origin git@bitbucket.org:USERNAME/PROJECT_NAME.git
~~~~

Once you have cloned the repository change into the directory where you cloned to and install node packages
~~~~
cd MYAPPLICATIONNAME
npm install
~~~~

And install frontend all bower components
~~~~
bower install
~~~~

# Starting Grunt and developing
Once bower and node packages have been installed start grunt using
~~~~
grunt
~~~~

This will start watching changes in style.scss file, all .js, .html and .scss files located anywhere in the 'build/components' folder, Any changes will automatically trigger SCSS to compile, Javascript to concatenate and HTML to be made into angular templates, this will also start the livereload service in your browser window.
You will be able to run your application and view your changes in realtime in the browser by visiting http://0.0.0.0:9001 or http://localhost:9001

# App and Boilerplate Structure
~~~~
/build/components/a_config/BoilerplateConfiguration.js
~~~~
Boilerplate specific configuration and bundle of important module dependencies

~~~~
/build/components/a_config/AppConfiguration.js
~~~~
Your primary app configuration.  This is where you have your routes listed, and where you should add your app specifics like app.run, app.config.  The routes in this file intended to be a lean quick listing of pages.  

# Route Structure
All routes html, scss, js is grouped together, for example:
~~~~
/build/components/routes/home.html
/build/components/routes/home.scss
/build/components/routes/HomeController.js
~~~~
HomeController.js file is where you should put:
1. Controller function
2. route.data
3. route.resolve
4. route.params


# Packaging for final distribution
~~~~
grunt build
~~~~

This will concatenate, compile, compress, minify and copy your publishable application into the 'dist' directory ready to be deployed


# Deploying to Fluro
You can now commit and push to your remote git repository.
Then create or redeploy an existing deployment at https://admin.fluro.io/deployment


Happy coding!
